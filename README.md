# discount
the client mod for people who don't like client mods.

copy pasted code from [powercord](https://github.com/powercord-org/powercord/)

## usage

- open devtools snippet panel
- create snippet called `boot`
  - this is your entrypoint. this snippet will be called on startup once the page finished loading.
- all snippets will be loaded into the `window.snippets` variable, using the format
```json
{
  "snippet name": "content" (string)
}
```

## examples!

heres an example boot snippet. you dont have to use this but its how I manage all my snippets that should get executed on startup.

#### boot
```js
window.sd = {
  registry: [],
  error: []
}

window.launchSnippet = function(id){
  if(!window.snippets[id]) throw `Invalid Snippet ${id}`
  try{
    eval(window.snippets[id])
    window.sd.registry.push(id)
  }catch(e){
    console.log('%c[boot] %cError while launching ' + id + ':', 'color: #FF5938', '');
    window.sd.error.push(id)
    console.log(e)
  }
}

function postLaunch(){
  try{
    // Launch boot snippets
    launchSnippet("boot_snippets")

    if(window.sd.error.length == 0) {
      console.log(`%c[boot] %cLaunched %c${sd.registry.length}%c snippets.`, 'color: #FF5938', '','color: #FFD34F','');
    } else {
      console.log(`%c[boot] %cLaunched %c${sd.registry.length}%c snippets with %c${sd.error.length}%c errors.`, 'color: #FF5938', '','color: #FFD34F','', 'color: #FF5160', '');
    }

    console.log('%c[boot] %cdelayed boot script executed.', 'color: #FF5938', '');

  }catch(e){
    setTimeout(() => {
      postLaunch()
    }, 2500)
  }
}

setTimeout(() => {
  postLaunch()
}, 2500)
```

#### boot_snippets
```js
launchSnippet("default-functions") //example, change the string to the name of the snippet you want to run
```
