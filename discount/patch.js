const Module = require('module');
const { join, dirname } = require('path');
const { existsSync, unlinkSync } = require('fs');

const electronPath = require.resolve('electron');
const discordPath = join(dirname(require.main.filename), '..', 'app.asar');
require.main.filename = join(discordPath, 'app_bootstrap/index.js');

require('./ipc/main');

const electron = require('electron');
const PatchedBrowserWindow = require('./window');

let _patched = false;
const appSetAppUserModelId = electron.app.setAppUserModelId;
function setAppUserModelId(...args) {

  appSetAppUserModelId.apply(this, args);
  if (!_patched) {
    _patched = true;
    require('./updater.win32.js');
  }
}

electron.app.setAppUserModelId = setAppUserModelId;

const electronExports = new Proxy(electron, {
  get(target, prop) {
    switch (prop) {
      case 'BrowserWindow': return PatchedBrowserWindow;

      case 'default': return electronExports;
      case '__esModule': return true;
      
      default: return target[prop];
    }
  }
});

delete require.cache[electronPath].exports;
require.cache[electronPath].exports = electronExports;

electron.app.once('ready', () => {
});

const discordPackage = require(join(discordPath, 'package.json'));
electron.app.setAppPath(discordPath);
electron.app.name = discordPackage.name;

console.log('starting the cord...');
Module._load(join(discordPath, discordPackage.main), null, true);
