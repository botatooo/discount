const { webFrame } = require('electron');
const { join } = require('path');
const { readFileSync } = require('fs')

const { ipcRenderer } = require('electron');
require('./ipc/renderer')

console.log('%c[discount] %cstarting...', 'color: #bada55', '');

// Get storage path depending on platform
let discordStoragePath;
switch (process.platform) {
  case 'win32':
    discordStoragePath = join(process.env.APPDATA, 'discordcanary');
    break;
  case 'linux':
    discordStoragePath = join(process.env.HOME, '.config', 'discordcanary');
    break;
  case 'darwin':
    discordStoragePath = join(process.env.HOME, 'Library', 'Application Support', 'discordcanary');
    break;
  default:
    discordStoragePath = process.env.DISCORD_STORAGE_PATH;
    break;
}

// Load snippets into window.snippets
let prefs = readFileSync(join(discordStoragePath, 'Preferences'), 'utf-8')
prefs = JSON.parse(prefs)
let snippets = JSON.parse(prefs.electron.devtools.preferences.scriptSnippets)
let out = {}
for(const snip of snippets){
  out[snip.name] = snip.content
}
webFrame.top.context.window.snippets = out;

window.addEventListener("load", function(){
  webFrame.executeJavaScript(out["boot"])
  console.log('%c[discount] %claunched boot snippet.', 'color: #bada55', '');
});

console.log(`%c[discount] %cready with %c${Object.keys(out).length} %csnippets.`, 'color: #bada55', '', 'color: #FFD34F', '');

const preload = ipcRenderer.sendSync('PRELOAD');
if (preload) {
  require(preload);
}
