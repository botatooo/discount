const { join } = require('path');
const { existsSync } = require('fs');
const { inject } = require('../inj/main');

if (process.platform === 'win32') {
  const injector = require(`../inj/${process.platform}`);
  const buildInfoFile = join(process.resourcesPath, 'build_info.json');

  const buildInfo = require(buildInfoFile);
  if (buildInfo && buildInfo.newUpdater) {
    const autoStartScript = join(require.main.filename, '..', 'autoStart', 'index.js');
    const { update } = require(autoStartScript);

    require.cache[autoStartScript].exports.update = async (callback) => {
      const appDir = await injector.getAppDir();
      if (!existsSync(appDir)) {
        return inject(injector).then(() => {
          update(callback);
        });
      }
    };
  } else {
    const hostUpdaterScript = join(require.main.filename, '..', 'hostUpdater.js');
    const { quitAndInstall } = require(hostUpdaterScript);

    require.cache[hostUpdaterScript].exports.quitAndInstall = () => {
      inject(injector).then(() => {
        quitAndInstall.call({ updateVersion: require.cache[hostUpdaterScript].exports.updateVersion });
      });
    };
  }
}
