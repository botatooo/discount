const { join } = require('path');
const { BrowserWindow } = require('electron');

let transparency = false;
let ewp = false;

class PatchedBrowserWindow extends BrowserWindow {
  constructor(opts) {
    let originalPreload;
    if (opts.webContents) {
    } else if (opts.webPreferences && opts.webPreferences.offscreen) {
      originalPreload = opts.webPreferences.preload;
    } else if (opts.webPreferences && opts.webPreferences.preload) {
      originalPreload = opts.webPreferences.preload;
      if (opts.webPreferences.nativeWindowOpen) {
        opts.webPreferences.preload = join(__dirname, './preload.js');
        opts.webPreferences.contextIsolation = false; // shrug
      }

      if (transparency) {
        opts.transparent = true;
        opts.frame = process.platform === 'win32' ? false : opts.frame;
        delete opts.backgroundColor;
      }

      if (ewp) {
        opts.webPreferences.experimentalFeatures = true;
      }
    }


    const win = new BrowserWindow(opts);
    win.webContents.__preload = originalPreload;
    return win;
  }

}

module.exports = PatchedBrowserWindow;
