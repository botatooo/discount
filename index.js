const main = require('./inj/main.js');
const discount = `\x1b[32m[discount]\x1b[0m`

console.log(`${discount} discount v1.0.1`);

let platformModule;
try {
  platformModule = require(`./inj/${process.platform}.js`);
  console.log(`${discount} detetected \x1b[33m${process.platform}\x1b[0m.`);
} catch (err) {
  console.log(`${discount} platform \x1b[31m${process.platform}\x1b[0m is not supported.`);
};

(async () => {
  if (process.argv[2] === 'inject') {
    console.log(`${discount} injecting...`);
    await main.inject(platformModule)
    console.log(`${discount} done.`);
  } else if (process.argv[2] === 'uninject') {
    console.log(`${discount} uninjecting...`);
    await main.uninject(platformModule)
    console.log(`${discount} done.`);
  }
})();
