const { existsSync } = require('fs');
const { readdir, lstat, unlink, rmdir } = require('fs').promises;
const discount = `\x1b[32m[discount]\x1b[0m`

const rmdirRf = async (path) => {
  if (existsSync(path)) {
    const files = await readdir(path);
    for (const file of files) {
      const curPath = `${path}/${file}`;
      const stat = await lstat(curPath);

      if (stat.isDirectory()) {
        await rmdirRf(curPath);
      } else {
        await unlink(curPath);
      }
    }
    await rmdir(path);
  }
};

const { mkdir, writeFile } = require('fs').promises;
const { join, sep } = require('path');

exports.inject = async ({ getAppDir }) => {
  const appDir = await getAppDir();
  if (existsSync(appDir)) {
    console.log(`${discount} \x1b[31merror:\x1b[0m uninject discount before reinjecting.`);
    return false;
  }

  await mkdir(appDir);
  await Promise.all([
    writeFile(
      join(appDir, 'index.js'),
      `require(\`${__dirname.replace(RegExp(sep.repeat(2), 'g'), '/')}/../discount/patch.js\`)`
    ),
    writeFile(
      join(appDir, 'package.json'),
      JSON.stringify({
        main: 'index.js',
        name: 'discord'
      })
    )
  ]);

  return true;
};

exports.uninject = async ({ getAppDir }) => {
  const appDir = await getAppDir();

  if (!existsSync(appDir)) {
    console.log(`${discount} \x1b[31merror:\x1b[0m nothing left to uninject.`);
    return false;
  }

  await rmdirRf(appDir);
  return true;
};
